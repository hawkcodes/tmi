<?php

namespace HawkCode\Tmi;

class Message
{
    private ?string $type;
    private ?string $content;
    private ?string $username;
    private ?string $channel;

    public function __construct(string $message)
    {
        $array = explode(' ', $message);
        $this->type = $array[2] ?? "";
        if ($this->type === 'PRIVMSG') {
            preg_match("/:([A-Z0-9_]+)!/i", $array[1], $matches);
            $this->username = $matches[1];
            $this->channel = ltrim($array[3], "#");
            $content = implode(" ", array_slice($array, 4));
            $this->content = trim(ltrim($content, ":"));
        } else {
            $this->username = "System";
            $this->type = "system";
            $this->content = $message;
        }
    }

    public function getChannel(): ?string
    {
        return $this->channel;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

}