<?php

namespace HawkCode\Tmi;

use Ratchet\Client\Connector;
use Ratchet\Client\WebSocket;
use React\EventLoop\Loop;
use React\EventLoop\LoopInterface;

class Client
{
    private LoopInterface $loop;

    private Connector $connector;

    private WebSocket $conn;

    private array $events;

    private ClientOptions $options;

    public function __construct(ClientOptions $options)
    {
        $this->options = $options;
        $this->loop = Loop::get();
        $this->connector = new Connector($this->loop);
    }

    public function connect()
    {
        ($this->connector)('wss://irc-ws.chat.twitch.tv:443')->then(function ($conn) {
            $this->conn = $conn;
            $conn->on('message', function ($msg) use ($conn) {
                $message = new Message($msg);
                echo "{$message->getUsername()}: {$message->getContent()}\n";
            });
            $conn->on('PING', function ($msg) use ($conn) {
                $conn->send('PONG');
            });
            $conn->on('close', function ($code = null, $reason = null) {
                echo "Connection closed ({$code} - {$reason})\n";
                $this->connect();
            });
            foreach ($this->events as $event) {
                $conn->on($event['event'], function ($msg) use ($event) {
                    $message = new Message($msg);
                    ($event['function'])($message);
                });
            }
            $this->login();
            foreach ($this->options->getChannels() as $i => $channel) {
                $this->join($channel);
                if ($i+1%19 == 0) {
                    sleep(10);
                }
            }


        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });
    }

    public function on(string $event, callable $function)
    {
        $this->events[] = [
            'event' => $event,
            'function' => $function
        ];
    }

    public function say(string $msg, string $channel)
    {
        $this->conn->send("PRIVMSG #{$channel} :{$msg}");
    }

    private function login()
    {
        $this->conn->send('PASS ' . $this->options->getPassword());
        $this->conn->send('NICK ' . $this->options->getNick());
    }

    private function join($channel)
    {
        $this->conn->send("JOIN #{$channel}");
        $this->conn->send('CAP REQ :twitch.tv/membership twitch.tv/tags twitch.tv/commands');
    }

}