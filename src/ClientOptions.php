<?php

namespace HawkCode\Tmi;

class ClientOptions
{
    private array $channels;
    private string $nick;
    private string $password;

    public function __construct(array $options)
    {
        $this->channels = $options['channels'];
        $this->nick = $options['oauth']['nick'];
        $this->password = $options['oauth']['password'];
    }

    public function getChannels(): array
    {
        return $this->channels;
    }

    public function getNick(): string
    {
        return $this->nick;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}